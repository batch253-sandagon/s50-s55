// Built-in react modules imports
import {useState, useEffect} from "react";

// Package imports
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
// Component imports
import "./App.css";
import AppNavbar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './components/CourseView';
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";

import {UserProvider} from "./UserContext";

// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process
// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page
function App() {
  /*
    - JSX syntax requires that components should always have closing tags.
    - In the example above, since the "AppNavbar" component does not require any text to be placed in between, we add a "/" at the end to signify that they are self-closing tags.
  */
  /*
    - In React JS, multiple components rendered in a single component should be wrapped in a parent component.
    - Not doing so will return an error in our application.
    - The "Fragment" component ensures that this error can be prevented.
  */

    // State hook for the user state that's defined here for a global scope
    // Initialized as an object with properties from the localStorage
    // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    const unsetUser = () => {
      localStorage.clear();
    };

    // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
    useEffect(() => {

      console.log(user);
      console.log(localStorage);
    }, [user]);

    return (
      // Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop.
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
            <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/courses" element={<Courses />} />
              <Route path="/courses/:courseId" element={<CourseView />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<Home isNotFound={true}/>} />
            </Routes>
            </Container>
        </Router>
      </UserProvider>
    );
}

export default App;
