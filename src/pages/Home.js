// Component imports
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

function Home({isNotFound}) {
  return (
    <>
      <Banner 
        title="Zuitt Coding Bootcamp" 
        subtitle="Opportunities for everyone, everywhere." 
        buttonText="Enroll Now!" 
        buttonLink="/courses"
        isNotFound={isNotFound}
      />
      {isNotFound ?
       "" : <Highlights />
      }
    </>
  );
};

export default Home;