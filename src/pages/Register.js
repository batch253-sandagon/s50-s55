import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate, useNavigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

function Register() {

    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // State to determine whether submit button is enabled or not
    const [isActive, setActive] = useState(false);

    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

        if (
            firstName !== "" &&
            lastName !== "" &&
            email !== "" &&
            mobileNumber !== "" &&
            mobileNumber.length === 11 &&
            password1 !== "" &&
            password2 !== "" &&
            password1 === password2
        ) {
            fetch(`http://localhost:4000/users/checkEmail`, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({ email: email }),
            })
              .then((res) => res.json())
              .then((data) => {
                console.log(data)
                if (data) {
                  Swal.fire({
                    title: "Duplicate email found! -Rnld",
                    icon: "error",
                    text: "Please provide a different email."
                  });
                } else {
                  fetch(`http://localhost:4000/users/register`, {
                    method: "POST",
                    headers: {
                      "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                      firstName: firstName,
                      lastName: lastName,
                      email: email,
                      mobileNo: mobileNumber,
                      password: password1,
                    }),
                  })
                    .then((res) => res.json())
                    .then((data) => {
                      if (data) {
                        Swal.fire({
                          title: "Registration successful! -Rnld",
                          icon: "success",
                          text: "Welcome to Zuitt!",
                        });
                        navigate("/login");
                      } else {
                        Swal.fire({
                          title: "Registration unsuccessful",
                          icon: "error",
                          text: "Please try again!",
                        });
                      }
                    });
                }
              });      
            setActive(true);
        } else {
            setActive(false);
        }    
      

        // Clear input fields
        setFirstName("");
        setLastName("");
        setEmail("");
        setMobileNumber("");
        setPassword1("");
        setPassword2("");
    };

    // Validation to enable submit button when all fields are populated and both passwords match
    useEffect(() => {
        if ((
            firstName !== "" &&
            lastName !== "" &&
            mobileNumber !== "" &&
            mobileNumber.length === 11 &&
            email !== "" && 
            password1 !== "" && 
            password2 !== "") && 
            (password1 === password2)) {
                setActive(true);
        } else {
            setActive(false);
        };
    }, [firstName, lastName, mobileNumber, email, password1, password2]);
    return (
        (user.id) ? 
            <Navigate to="/course"/>
        :
            // 2-way Binding (Bnding the user input states into their corresponding input fields via the onChange JSX Event Handler)
            <Form onSubmit={e => registerUser(e)}>
            <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="name" 
                    placeholder="Enter your first name" 
                    value={firstName} 
                    onChange={e => {setFirstName(e.target.value)}} 
                    required/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="name" 
                    placeholder="Enter your last name" 
                    value={lastName} 
                    onChange={e => {setLastName(e.target.value)}} 
                    required/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email} 
                    onChange={e => {setEmail(e.target.value)}} 
                    required/>
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Enter your mobile number" 
                    value={mobileNumber} 
                    onChange={e => {setMobileNumber(e.target.value)}} 
                    required/>
            </Form.Group>


            <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1} 
                    onChange={e => {setPassword1(e.target.value)}} 
                    required/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2} 
                    onChange={e => {setPassword2(e.target.value)}} 
                    required/>
            </Form.Group>

            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
            : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
            }
            </Form>
    );
};

export default Register;