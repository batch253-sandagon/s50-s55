import { useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";
// import mock database
// import coursesData from "../data/courses";

function Courses() {
    
    const [ courses, setCourses] = useState([]);

    useEffect(() => {
        fetch(`http://localhost:4000/courses`)
        .then(res => res.json())
        .then(data => {
            setCourses(data);

            setCourses(data.map(course => {
                return (
                    <CourseCard key={ course._id } courseProp={course}/>
                )
            }))
        })
    }, [])

    // The "course" in the CourseCard component is called a "prop" which is a shorthand for "property" since components are considered as objects in React JS
    // The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ("")
    // We can pass information from one component to another using props. This is referred to as "props drilling"
    

    return (
        <>
            {courses}
        </>
    );
};

export default Courses;