import { Navigate } from "react-router-dom";
import {useContext, useEffect} from "react";
import UserContext from "../UserContext";

function Logout() {
    const { unsetUser, setUser } = useContext(UserContext);
    
    unsetUser();

    useEffect(() => {
        setUser({id:null})
    });
    
    // localStorage.clear(); - already implemented in App.js

    // Redirect back to login
    return (

        <Navigate to="/login"/>
    );
};

export default Logout;