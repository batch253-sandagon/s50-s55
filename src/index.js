import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';

import "bootstrap/dist/css/bootstrap.min.css";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// const name = "Owen Orange";
// const user = {
//   firstName: "Lulu",
//   lastName: "Tamayo"
// }

// function formatName(user){
//   return user.firstName + user.lastName;
// }

// // const element = <h1>Hello, {name}</h1>
// const element = <h1>Hello, {formatName(user)}</h1>

// const root = ReactDOM.createRoot(document.querySelector("#root"));

// root.render(element);