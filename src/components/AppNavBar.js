import { useContext } from 'react';
import {Container, Navbar, Nav} from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

import UserContext from "../UserContext";

function AppNavBar() {

  // State to store the user information stored in the login page.
  // const [user, setUser] = useState(localStorage.getItem("email"));
  // console.log(setUser);

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg" className="mx-5">
        <Container fluid>
            <Navbar.Brand as={ Link } to="/">Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"></Navbar.Toggle>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto">
                    {/*The "as" prop allows components to be treated as if they are a different component gaining access to it's properties and functionalities.*/}
                    {/*- The "to" prop is used in place of the "href" prop for providing the URL for the page.*/}
                    {/*- The "exact" prop is used to highlight the active NavLink component that matches the URL.*/}
                    <Nav.Link as={ NavLink } to="/">Home</Nav.Link>
                    <Nav.Link as={ NavLink } to="/courses">Courses</Nav.Link>

                    { (user.id) ? 
                      <Nav.Link as={ NavLink } to="/logout">Logout</Nav.Link>
                    :
                      <>
                        <Nav.Link as={ NavLink } to="/login">Login</Nav.Link>
                        <Nav.Link as={ NavLink } to="/register">Register</Nav.Link>
                      </>
                    }
                </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
  )
}

export default AppNavBar;
