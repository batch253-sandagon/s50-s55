import React from 'react';
import { Button, Row, Col} from "react-bootstrap";

function Banner({ title, subtitle, buttonText, buttonLink, isNotFound }) {
  return (
    <Row>
      <Col className="p-5">
        {isNotFound ? (
          <>
            <h1>Page Not Found</h1>
            <p>Go back to the homepage.</p>
            <Button variant="primary" href="/">Go Back Home</Button>
          </>
        ) : (
          <>
            <h1>{title}</h1>
            <p>{subtitle}</p>
            <Button variant="primary" href={buttonLink}>{buttonText}</Button>
          </>
        )}
      </Col>
    </Row>
  );
}

export default Banner;